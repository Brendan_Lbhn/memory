window.onload= init()

function init() {
    document.getElementById('choixPlateau').addEventListener('input',afficherImage);
}

let userName = sessionStorage.getItem('currentUser')
let email = sessionStorage.getItem('currentMail')
document.getElementById('titrePage').innerText = 'Bienvenue ' + userName; 
document.getElementById('identifiant').innerText = userName; 
document.getElementById('mail').innerText = email; 


function afficherImage(){
    let     choix = document.getElementById('choixPlateau').value;
    switch(choix) {
        case '1' :
            document.getElementById('emplacementImage').innerHTML = "<img src='images/alphabet-scrabble/memory_detail_scrabble.png' alt=''>";
            tailleGrille(choix);
            break
        case '2' :
            document.getElementById('emplacementImage').innerHTML = "<img src='images/animaux/memory_detail_animaux.png' alt=''>";
            tailleGrille(choix);
            break
        case '3' :
            document.getElementById('emplacementImage').innerHTML = "<img src='images/animauxAnimes/memory_detail_animaux_animes.png' alt=''>";
            tailleGrille(choix);
            break
        case '4' :
            document.getElementById('emplacementImage').innerHTML = "<img src='images/animauxdomestiques/memory_detail_animaux_domestiques.png' alt=''>";
            tailleGrille(choix);
            break
        case '5' :
            document.getElementById('emplacementImage').innerHTML = "<img src='images/chiens/memory_details_chiens.png' alt=''>";
            tailleGrille(choix);
            break
        case '6' :
            document.getElementById('emplacementImage').innerHTML = "<img src='images/dinosaures/memory_detail_dinosaures.png' alt=''>";
            tailleGrille(choix);
            break
        case '7' :
            document.getElementById('emplacementImage').innerHTML = "<img src='images/memory-legume/memory_detail.png' alt=''>";
            tailleGrille(choix);
            break   
        case '0' : 
            return
    }
}


function tailleGrille (choix){
    switch(choix) {
        case '1' :
            document.getElementById('tailleGrille').innerHTML = "<option value='0'>Sélectionnez une taille de Grille</option><option value='1'>2 x 2</option><option value='2'>3 x 3</option><option value='3'>4 x 4</option><option value='4'>5 x 5</option><option value='5'>5 x 6</option><option value='6'>6 x 6</option><option value='7'>7 x 7</option>";
            break
        case '2' :
            document.getElementById('tailleGrille').innerHTML = "<option value='0'>Sélectionnez une taille de Grille</option><option value='1'>2 x 2</option><option value='2'>3 x 3</option><option value='3'>4 x 4</option><option value='4'>5 x 5</option>";
            break
        case '3' :
            document.getElementById('tailleGrille').innerHTML = "<option value='0'>Sélectionnez une taille de Grille</option><option value='1'>2 x 2</option><option value='2'>2 x 3</option><option value='3'>2 x 4</option>";
            break
        case '4' :
            document.getElementById('tailleGrille').innerHTML = "<option value='0'>Sélectionnez une taille de Grille</option><option value='1'>2 x 2</option><option value='2'>2 x 3</option><option value='3'>2 x 5</option><option value='4'>3 x 3</option>";
            break
        case '5' :
            document.getElementById('tailleGrille').innerHTML = "<option value='0'>Sélectionnez une taille de Grille</option><option value='1'>2 x 2</option><option value='2'>2 x 3</option><option value='3'>2 x 5</option><option value='4'>3 x 3</option><option value='5'>4 x 4</option><option value='4'>5 x 4</option>";
            break
        case '6' :
            document.getElementById('tailleGrille').innerHTML = "<option value='0'>Sélectionnez une taille de Grille</option><option value='1'>2 x 2</option><option value='2'>2 x 3</option><option value='3'>2 x 5</option><option value='4'>3 x 3</option>";
            break
        case '7' :
            document.getElementById('tailleGrille').innerHTML = "<option value='0'>Sélectionnez une taille de Grille</option><option value='1'>2 x 2</option><option value='2'>2 x 3</option>";
            break
        case '0' : 
            return
    }
}
