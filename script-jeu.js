window.onload = init()

function init(){
    document.querySelector('button').addEventListener('click',melangerCartes);
    window.addEventListener('keydown',raffraichirEcran);
    document.getElementById('imageVictoire').addEventListener('click',cacherVictoire)
}

const NB_CARTES_INITIAL= 6;
let nbClic = 0;
let score = 0;
let pairesTrouvees = 0
document.getElementById('score').innerText = '0';
document.getElementById('nbPaires').innerText = pairesTrouvees +  ' / ' + NB_CARTES_INITIAL;

//crée les doubles de cartes, en attribuant une classe commune entre les deux cartes de la paire,
//et en ajoutant un id unique a chaque nouvelle carte
for(let i=1; i < NB_CARTES_INITIAL+1; i++){
    let carteClonee = document.getElementById('img'+i)
    let clone = carteClonee.cloneNode(true);
    clone.setAttribute('id','img'+(NB_CARTES_INITIAL+i));
    clone.setAttribute('class','img'+i);
    document.getElementById('cards').appendChild(clone);
}

// ajoute un eventListener sur chaque carte
for(let i=1; i< (NB_CARTES_INITIAL*2) + 1; i++){
    document.getElementById('img' + i).addEventListener('click',retourneCarte)
}

//Mélange les cartes au clic sur ESPACE
function raffraichirEcran(event){
    if (event.key == " "){
        melangerCartes()
    }
}

//mélange les cartes
function melangerCartes(){
    for( let i=1; i < (NB_CARTES_INITIAL*2);i++){
        document.getElementById('img'+i).firstElementChild.setAttribute('class','cachee')
        document.getElementById('img'+i).lastElementChild.setAttribute('class','visible')
    }
    for( let i=1; i < (NB_CARTES_INITIAL*2)+1;i++){
        let positionCarte = Math.trunc(Math.random()*10)
        document.getElementById('img'+i).style.order = positionCarte
        document.getElementById('img'+i).firstElementChild.setAttribute('class','cachee')
        document.getElementById('img'+i).lastElementChild.setAttribute('class','visible')
    }
    score = 0;
    document.getElementById('score').innerText = score
    pairesTrouvees = 0;
    document.getElementById('nbPaires').innerText = pairesTrouvees
    nbClic = 0;
}
melangerCartes();

//retourne la carte au clic
function retourneCarte(event){
    let carteVisible = event.currentTarget.firstElementChild;
    let dosCarte = event.currentTarget.lastElementChild;
    let elem = event.currentTarget
    let classPremiereCarte;
    let classDeuxiemeCarte;
    document.getElementById('feedback').innerText = ''
    //premiere carte cliquée
    if (nbClic === 0){ 
        localStorage.setItem('idPremiereCarte',event.currentTarget.id);
        elem.animate(
            [{transform: 'scaleX(1)'}, {transform: 'scaleX(0)'}],
            {duration: 200})
            .onfinish = () => {
                carteVisible.setAttribute('class','visible');
                dosCarte.setAttribute('class','cachee');
                elem.animate(
                    [{transform: 'scaleX(0)'}, {transform: 'scaleX(1)'}],
                    {duration: 200});                
        };
        nbClic = 1
        classPremiereCarte = event.currentTarget.getAttribute('class')
        localStorage.setItem('classPremiereCarte', classPremiereCarte)
    //deuxième Carte cliquée
    }else {
        //Si on reclique sur la même carte
        if(localStorage.getItem('idPremiereCarte') === event.currentTarget.id){
            return
        //Si on clique sur une autre carte
        }else{
            localStorage.setItem('idDeuxiemeCarte',event.currentTarget.id )
            elem.animate(
                [{transform: 'scaleX(1)'}, {transform: 'scaleX(0)'}],
                {duration: 200})
                .on 
                    carteVisible.setAttribute('class','visible');
                    dosCarte.setAttribute('class','cachee');
                    elem.animate(
                        [{transform: 'scaleX(0)'}, {transform: 'scaleX(1)'}],
                        {duration: 200});                
            };            
            classDeuxiemeCarte = event.currentTarget.getAttribute('class')
            localStorage.setItem('classDeuxiemeCarte', classDeuxiemeCarte)
            nbClic = 0
            //si les deux cartes sont identiques
            if (localStorage.getItem('classPremiereCarte') === localStorage.getItem('classDeuxiemeCarte')){
                pairesTrouvees = pairesTrouvees + 1
                document.getElementById('nbPaires').innerText = pairesTrouvees + ' / ' + NB_CARTES_INITIAL;
                document.getElementById('feedback').innerText = 'Bravo, tu as trouvé une paire !';
                score = score+1;
                document.getElementById('score').innerText = score;
                //Si toutes les paires ont été trouvées
                if (pairesTrouvees === NB_CARTES_INITIAL){
                    document.getElementById('feedback').innerText = 'Bravo BG, tu as gagné !';
                    document.getElementById('imageVictoire').setAttribute('class','imageVictoireVisible')
                }
            //Si les deux cartes sélectionnées ne sont pas les mêmes
            }else{
                score = score+1;
                document.getElementById('score').innerText = score;
                document.getElementById('feedback').innerText = 'Essaye encore !'
                document.getElementById('jeuMain').style.pointerEvents = 'none';
                setTimeout(cacheCartes,2000,carteVisible,dosCarte)
            }    
        }  
    }

function cacheCartes(carteVisible,dosCarte){
    document.getElementById(localStorage.getItem('idPremiereCarte')).firstElementChild.setAttribute('class','cachee');
    document.getElementById(localStorage.getItem('idPremiereCarte')).lastElementChild.setAttribute('class','visible');
    carteVisible.setAttribute('class','cachee');
    dosCarte.setAttribute('class','visible');
    document.getElementById('jeuMain').style.pointerEvents = 'all'; 
}

function cacherVictoire(){
    document.getElementById('imageVictoire').setAttribute('class','imageVictoireInvisible')
}
