window.onload= init()

function init(){
    document.getElementById('userName').addEventListener('input', verifierNom);
    document.getElementById('email').addEventListener('input',verifierMail);
    document.getElementById('password').addEventListener('input', verifierMdP);
    document.getElementById('passwordConfirm').addEventListener('input',verifierMdpConfirm);
    document.forms[0].addEventListener('submit', enregistrerUser);
    window.addEventListener('input', validation)    
}

let nomUtilisateur = false
let mail = false
let mdp = false


function verifierNom(event){
    let saisie = event.currentTarget.value;
    if (saisie.length >=3){
        document.getElementById('nameOk').setAttribute('class','indicateurVisible');
        document.getElementById('nameError').setAttribute('class','indicateurInvisible');
        nomUtilisateur = true;
    }else{
        document.getElementById('nameOk').setAttribute('class','indicateurInvisible');
        document.getElementById('nameError').setAttribute('class','indicateurVisible');
    }
}

function verifierMail(event){
    let saisie = event.currentTarget.value; 
    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(saisie)){
        document.getElementById('mailOk').setAttribute('class','indicateurVisible');
        document.getElementById('mailError').setAttribute('class','indicateurInvisible');
        mail = true;
    }else{
        document.getElementById('mailOk').setAttribute('class','indicateurInvisible');
        document.getElementById('mailError').setAttribute('class','indicateurVisible');
    }
}


function verifierMdP(event){
    let saisie = event.currentTarget.value;
    let symboleOk = verifierSymbole(saisie);
    let chiffreOk = verifierChiffre(saisie);
    let caracteresOk = verifierCaracteres(saisie);

    if(caracteresOk && chiffreOk){
        document.getElementById('mdpMoyen').setAttribute('class','mdpMoyenVisible');
    }else if (caracteresOk && symboleOk) {
        document.getElementById('mdpMoyen').setAttribute('class','mdpMoyenVisible');
    }else{
        document.getElementById('mdpMoyen').setAttribute('class','indicateurGrise');
    }

    if(saisie.length >= 9 && symboleOk && chiffreOk){
        document.getElementById('mdpFort').setAttribute('class','mdpFortVisible');
    }else{
        document.getElementById('mdpFort').setAttribute('class','indicateurGrise');
    }

    if(symboleOk && chiffreOk && caracteresOk){
        document.getElementById('mdpOk').setAttribute('class','indicateurVisible');
        document.getElementById('mdpError').setAttribute('class','indicateurInvisible');
        mdp = true
    }else{
        document.getElementById('mdpOk').setAttribute('class','indicateurInvisible');
        document.getElementById('mdpError').setAttribute('class','indicateurVisible');
    }

}

function verifierMdpConfirm(event){
    let saisie = event.currentTarget.value;
    if(document.getElementById('password').value === saisie){
        document.getElementById('mdpConfirmOk').setAttribute('class','indicateurVisible');
        document.getElementById('mdpConfirmError').setAttribute('class','indicateurInvisible');
    }else{
        document.getElementById('mdpConfirmOk').setAttribute('class','indicateurInvisible');
        document.getElementById('mdpConfirmError').setAttribute('class','indicateurVisible');
    }
}

function verifierSymbole(saisie){
    if (saisie.match(/[!-/]/g) != null){
        return true;
    }
    else if(saisie.match(/[:-@]/g) != null){
        return true;
    }else if (saisie.match(/[[-`]/g) != null){
        return true;
    }else if(saisie.match(/{:-˜]/g) != null){
        return true;
    }else{
        return false;
    }
}

function verifierChiffre(saisie){
    return saisie.match(/[0-9]/g) != null;
}

function verifierCaracteres(saisie){
    if (saisie.length >= 1) {
        document.getElementById('mdpFaible').setAttribute('class','mdpFaibleVisible');
    }else{
        document.getElementById('mdpFaible').setAttribute('class','indicateurGrise');
    }
    if(saisie.length >= 6){
        return true
    }else{
        return false
    }
}

function enregistrerUser(event){
    event.preventDefault();
    let userName = document.getElementById('userName').value;
    localStorage.setItem('userName', userName);
    let email = document.getElementById('email').value; 
    localStorage.setItem('email', email);
    let password = document.getElementById('password').value;
    localStorage.setItem('password',password);
    window.location.href='http://127.0.0.1:5500/connexion.html'
}

function validation(){
    if (nomUtilisateur && mail && mdp){
        document.getElementById('valider').setAttribute('class', 'valider')
        document.getElementById('valider').removeAttribute('disabled');
    }
}