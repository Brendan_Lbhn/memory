window.onload = init()

function init(){
    document.getElementById('userName').addEventListener('input',verifUsername)
    document.getElementById('password').addEventListener('input',verifPassword)
    document.forms[0].addEventListener('submit', connexion)
}

let userNameOk = false;
let passwordOK = false;
let connexionOk = false


function verifUsername(){
    let saisie = document.getElementById('userName').value
    if(localStorage.getItem('userName') === saisie){
        userNameOk = true
    }

}

function verifPassword(){
    let saisie = document.getElementById('password').value
    if(localStorage.getItem('password') === saisie){
        passwordOK = true;
    }

}

function connexion(event){
    if(userNameOk && passwordOK){
        event.preventDefault();
        sessionStorage.setItem('currentUser',document.getElementById('userName').value);
        sessionStorage.setItem('currentMail',localStorage.getItem('email'))
        window.location.href = 'http://127.0.0.1:5500/profil.html'
    }else{
        alert("J'te connais pas gros !")
    }

}